#Tasks:





⁃	inventory and how to use things



⁃	Clear this stuff


![image|20%](./img/01cmpstpile.jpg)



⁃	Dig out 3-4 inch and gravel


![image](./img/02gravel-sml-ada.jpeg.jpg)

![image](./img/03-ada.jpeg)


(Finish this task on same day as beginning)



⁃	Sheet mulching

![image](./img/04sheet.jpeg)


⁃	1/4 inch minus gravel needed for fill in ADA



![image](./img/05gravel.jpeg)



⁃	cleaning pathways and weeding and weed ID path maintenance if extra work needed



⁃	Move compost pile from here


![image](./img/06compst.jpeg)




To there


![image](./img/07-cmpst.jpeg)




⁃	cut this pile's biggest pieces with loppers


![image](./img/08lpprs.jpeg)



⁃	talk to Cherry about plant work on BIPOC



⁃	Gravel project to even out walkway at stairs (use bigger gravel)


![image](img/09stairsgravel.jpeg)

![image](img/10stairs.jpeg)

![image](img/11stairs.jpeg)



Black cap raspberry


![image](./img/12blkcprspbrry.jpeg)




StrWberry tree


![image](./img/13strbrrytree.jpeg)



Corkscrew willow


![image](./img/14crkscrwillow.jpeg)




sheet mulch (is tarweed) 


![image](./img/15trweed.jpeg)




⁃	Fork out the grass


![image](./img/16frkgrss.jpeg)


![image](./img/17grss.jpeg)



Sorrel


![image](./img/18srrl.jpeg)



⁃	clear out of grass and weeds


![image](./img/19clrgrss_weeds.jpeg)



If something had been cut up it will be growing is a plant yo keep


![image](./img/20cutstlks.jpeg)



⁃	clear weeds from trees and make awell


![image](./img/21treewll.jpeg)



⁃	Cut mint to ground and weed around it


![image](./img/22cutarndmnt.jpeg)



⁃	Pruning grape vine to second or third knob; leave a couple of buds


![image](./img/23grpstlk.jpeg)



⁃	szechuan pepper


![image](./img/24szechuanpppr.jpeg)



⁃	Clear buttercup in guilds


![image](./img/25bttrcpgld.jpeg)



⁃	Pear, goji, fig (top to bottom)


![image](./img/26prgjifig.jpeg)



⁃	example of guild work


![image](./img/27hzlnutgldxmpl.jpeg)
