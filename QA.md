# Questions and my (D, 1st Arbiter) answers

### These are questions brought up in the email thread where I am canvassing for feedback from the community. I will compile questions and concerns in this file and address them as time permits.


1. It doesn't seem that the BFF Discord is used very regularly, even though a decent number of people have accounts. Does that say anything about how readily people would use GitLab? If there is not a critical mass of regular BFF volunteers who are willing to migrate away from using email as a primary communication mechanism, it will be difficult to transition to something new, because there won't be much useful information available on the new platform even if many new people are eager to use the new platform

```
I hear you-- the discord is not as popping as it could be. I think that timing and organization of the the channels and workflows were well suited to a "drop-in-and-out" approach but the time has come to optimize the pipelines of project management and effective team communication. With that, I propose that something like the design I have put below

The parts highlighted in red are "gitlab channels", which would house only notifications from corresponding GitLab projects. I.e. Beekeeping Discord group will have access to the beekeeping <gitlab-updates> channel, which sends all notifications from the Beekeeping GitLab project to that Discord channel.

A Discord that prioritizes the workflows that best serve the community would be optimal in order for there to be utmost transparency in using GitLab, but it is not a requirement. I am merely an IT professional who is telling you what works in terms of completion of up-to-date information and the transparency thereof. A robust messaging system between a project workspace and physical work is an absolute necessity as teams grow. I plan for growth.

And something that must be said, I have mentioned the importance of digital security throughout the proposal and I will tell you again about BitWarden and to make an account even if you are not a fan of using GitLab. It is completely free and it will make your digital life not only safer but easier to get by in a secure manner.
```

![discord example](./discord_ex.png)



2. Does BFF really need project management and file version control software? I wonder if there is enough need to justify the learning curve and challenges in getting people to adopt a new platform. With normal turnover in staff and volunteers, there will be an ongoing burden of training people to use software if it's something that most people aren't familiar with, so it's not just a matter of getting people up to speed one time.

```
Let's look at the current form of communication/project planning in Beacon Food Forest:

1. Person A sends an email to sitedev committee to start planning an event.
2. Person B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y & Z all receive emails to their inboxes in the 1 minute after Person A had sent the event planning email.


Let's look at the communication challenge you face when, say, having a dedicated channel for "Event Planning" in Discord and Person A wants to plan an event:

1. Person A posts their event plans in BFF Discord "Event Planning" channel
2. Person B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y & Z have no event planning task sitting in their email inbox, it is sitting in a dedicated channel meant for that specific function, and therefore may view it and reply to it asynchronously at their own leisure and do not have to dig through thready email chains in order to be effective in their work.


There is something to be said the inherent knowledge of most users in how to use email, but using Discord is not that far removed. You need an email account to get on Discord, anyway. You just don't have to clutter your inbox in order to have effective communication using it.


Regarding familiarity, this is why in the proposal I made clear that I would be willing to train whomever whenever and provided. The software itself does look daunting but the git protocol is universal among yes software teams, and teams who support software developers, i.e. non-software developers. Part of this proposal is to make joining and doing the base-work needed to be effective in using GitLab with a team as seemless as possible. We are all learning.
```



3. Is Gitlab the most suitable tool for BFF? I don't have the impression that most of the people volunteering at BFF work in software, so they may not be familiar with or motivated to learn a platform that is primarily intended for software development. Before adopting any particular platform for project management or version control, I think it would be useful to do a survey of current BFF volunteers to see what platforms they are familiar with and how willing they are to learn to use a new platform. It might be that there are other good platforms out there that are familiar to more people.

```
I would love to see the alternatives, now that we are talking about project management. As an IT consultant, I can tell you working through and organizing emails is not a great use of my time. I only do it because a community I care about is attached to the idea of email threads.

Again, it is worth mentioning that GitLab is a web platform for a version control system. That's it. It is typically used for software developing, but any team could use a version control system.
```



4. if Gitlab were used in onboarding new volunteers, can it be integrated with the website platform?

```
I am not sure the current process of onboarding via email perhaps Cherry could fill in. Isn't is just that they are signed up for an email list?

That said, we can invite people to a BFF GitLab instance by email which will then prompt them to create a free account.
```


5. If we are aiming for transparency and inclusion, we need to use platforms that are accessible to as many people as possible, regardless of age, occupation, income, etc....is Gitlab going to be accessible to a wide variety of people? 

```
It is a SaaS (Software as a Service) web platform. If you have internet, you have access to GitLab.
It is free. No payments whatsoever for a base account and 5GB of storage (similar to apple's iCloud limit)
Occupation/income/age - I have taught boomers how to use git protocol from the web, as well as non-rich folks (but also rich folks) and GitLab is, as I said, used internally at GitLab across teams including Finance, HR, accounting, Business System, Product Management, etc. 


This is all part of my mission to streamline onboarding for folks and get them working in BFF as quickly as possible. As things exist now, there is the ethereal "email groups" that will message every once in awhile as needs arise and those emails are forgotten about for all time unless someone goes out of their way to save them.

Discord & GitLab: teams exist in actual spaces. They have their own dedicated channels and roles/permissions, and in the inflow of information/updates to those teams has their functional inputs/outputs. 

Email: I am only a part of a team insofar as someone has told me that I was added to an email list by someone who I don't know. And I only know that it was successful and that the team is doing things when someone sends a message.

Discord & GitLab: upon joining a team I know immediately which projects & communications they have, what issues they face, previous issues they have faced in the past and who is working on what.

Emails: ¯\_(ツ)_/¯

Discord & GitLab: all previous issues and milestones are always saved per project. Therefore, the Planting team can keep track of all things planted, who did what work and when, issues they face (including tagged issues to make tracking types of issues easier) and how those issues relate to other projects (issues can be moved and/or duplicated between projects. i.e. issues of path maintenance that plant team is dealing with can be duplicated to the Site Development project in GitLab, and vice versa)

Emails: ¯\_(ツ)_/¯

Discord & GitLab: I can see who made changes to community files at exactly what time and which Arbiter/team approved said change and those changes are published to a Discord channel

Emails & Google Drive: ¯\_(ツ)_/¯
```


6. If the thought is to integrate Discord with Gitlab, I would want to see that the Discord is being well used before adding another platform (unless there is good reason to believe that integrating Gitlab with Discord would make people more likely to use Discord). I wonder if just linking to the Google Drive in Discord for working collaboratively on documents might be a good start, and if that went, well, then try moving to more elaborate document management / project management software.

```
I agree-- using Discord more would be ideal and more fun with BFF. This is why I have been working to convince the Discord admin of the good points of these integrations as well as a re-design of channels and workflows in Discord to better reflect the work that happens at BFF and the flow of team communication and needs for updates.

With Google Drive, I am sure that you are aware that there had been an issue with people either deleting or editing files that should not have been so, and nobody can answer as to why that happened and who did it. GitLab would eliminate that possibility-- changes made to community files are completely visible and the information complete (who made the change, what the changes are vs. what is saved, when the changes were made, etc. all can be sent to a discord channel to keep updated on these changes). Google Drive has no integration for something to that effect, at least to my knowledge. Unless you are familiar with Google API? If so, I would love to see the Proof of Concept of such an integration.

If you look into my proposal again you will see that I intend for Google Drive to be used as a permanent at-rest storage not "currently working on this" files unless we think it makes sense to save versions. We could, depending on the amount of Google Drive storage we have, store backups of the entire GitLab infrastructure and all files on Google Drive at all times so all working data is being edited and discussed in GitLab, and when that work is done the final versions of that work are stored in Google Drive. Kind of like the different between a desk and a warehouse. Google Drive is the warehouse. GitLab is the desk where we work on stuff before it is sent to warehouse upon completion.
```



# How Git Works

![how git protocol works](./git.png)


# How GitLab Works

[Go here for a description on GitLab Workflow](https://docs.gitlab.com/ee/topics/gitlab_flow.html)

![GitLab workflow](./gitlab_flow.png)
