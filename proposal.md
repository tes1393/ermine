# GitLab for Permaculture Proposal

David Frazier



### Summary

Beacon Food Forest is a Permaculture project with a great capacity for providing services for the local community. As it grows, membership & projects becomes more varied, numerous, & complex. With this, feedback loops between inputs/outputs become that much more important. BFF already has a wonderful Discord with asynchronous messaging and updating on community events and work, and we need an agile solution with high visibility of all changes to provide openings in the feedback loops we need in order to have the most cohesive and comprehensive experience in volunteering and completing work at the Beacon Food Forest. I believe a repository with version control and granular member/project maintenance is the solution, and for this I propose [GitLab](https://docs.gitlab.com/ee/tutorials/).


### Project Background

GitLab is free for most cases, though there are enterprise versions with extra features. It exists in the cloud as a web service, much like google docs. It is a web application that is meant, typically, for Software Developers and other technical engineers/analysts. That said, it is widely used internally at GitLab and throughout their customers for marketing, finance, product management, facilities, security, & many other career fields. It is an effective and highly transparent project management tool, complete with file and document storage, supporting many different filetypes including PDFs, images & [markdown](https://www.ionos.com/digitalguide/websites/web-development/markdown/). It is similar to Google Docs, but with very specific version control. For example, should you make a change to a file in a GitLab repository, then that change can be made, previewed, and that change will be "proposed" to the community and GitLab maintainers so that ***all*** members may see what files are being changed by whom and how. Additionally, proposed changes can be sent, as a notification, to email and/or to the BFF Discord for an extra layer of visibility to the community.


### Solutions & Approach

The goal is to create an online repository for projects and documents of the Beacon Food Forest. It's main credo is to provide transparent community changes for ***all*** people who are here to help and support BIPOC and the greater mission of BFF. A project management suite such as GitLab could reduce the need of emails to propose changes to any community file/document/policy when coupled with Discord, as the integration with Discord using webhooks is paramount in maintaining high visibility of work being done for BFF. This will also, and possibly more importantly, future proof the IT infrastructure of BFF. Generation Z is already on Discord and using it swimmingly, and they have great capacity for learning a tool such as GitLab, which would help make them more marketable in their careers. Combining the two would not only protect us from future problems of getting generation Z folk onboarded into the community, but it would eliminate the need for lengthy and hard-to-follow email threads, which can be a deterrent for younger folk who have less and less need for email in this digital age.


#### Why Gitlab?

- Project Management with issue tracking, issue tags/labels (for easier task management and getting analytics on what types of tasks are usually being done and by whom), definable groups/subgroups (e.g. `Beacon Food Forest group -> Stewardship subgroup -> water stewardship subgroup`)
- Does not require sharing of phone numbers, emails, or any personal information beyond what is in a user's gitlab account
- Users can be "invited" to the Beacon Food Forest GitLab from an administrator panel, making joining the community that much easier
- Version control - all changes made to all files/documents can be made visible to the whole of the community, and all users of the org may comment or request information on any specific change (even line-by-line) within each change proposal to allow conversations around specific changes and actions by any community member (if desired-- control is granular so we can edit permissions on specific projects, files, etc.)
- It has a Wiki for each project

#### How we would use Gitlab

- Use GitLab for "files under construction" in conjunction with more permanent storage remaining as Google Drive
- Continual projects with [trackable tasks/issues](https://about.gitlab.com/blog/2018/08/02/4-ways-to-use-gitlab-issue-boards/) (e.g. [using tags to track seasonal-type issues](https://www.cloudsavvyit.com/3365/how-to-use-gitlab-issues-for-tracking-software-development/))
- Proposed edit/addition of any file can be made to go through community verification **before** such a change is actually implemented (prevents permanently-sorted files in Google Drive from inadvertently being deleted as edits and re-saves occur)
- Integration of the Beacon Food Forest GitLab with Discord. [Specific repos, files, project changes (and all *proposed* changes) may be sent as a message notification to specific Discord channels of the Discord admin's choosing (e.g., changes made to files within a `Water Stewardship` project can send a notification to a designated `Water Stewardship Updates` channel on Discord)](https://docs.gitlab.com/ee/user/project/integrations/discord_notifications.html)
- Meaningful file editing. With the process of forking and merge requests, people who work on Beacon Food Forest files will rest easy knowing that changes made to files don't *just happen* but go through a review process. Below is a flowchart of the process of changing a community file.

![GitLab Workflow for BFF](./bff_gitlab_flow.pdf)

#### Plan for Launch

1. Evaluate what files are needed for project work and supporting documents that need to be moved to GitLab
2. Send information on signing up and usage of GitLab to the most active groups within the community
3. Make myself (D) available to 1-on-1 or group consultation and training in the usage of project management tools within GitLab
4. Use GitLab for at least one project 
5. Gather feedback from the group prototyping GitLab
6. Bring the feedback to the community and discuss
7. Hone what works/doesn't work for workflows and training
8. Make myself (D) available to 1-on-1 or group training based on feedback from community
9. Communicate to all groups via email about launch date and final comprehensive guide on signup & usage of GitLab for community projects

#### What you, a future BFF GitLab member need to know

This is a daunting tool, I get it. However, the benefits far outweigh the learning curve's pains. I have gone from 0% knowledge of [git protocol](https://git-scm.com/book/en/v2/Getting-Started-What-is-Git%3F) to admin level management because of repetition and thinking of how these development tools could help with my own projects, too.

That said here is the list of things that would be helpful in running GitLab in a secure manner:

1. [GitLab tutorials](https://docs.gitlab.com/ee/tutorials/)
2. Get some [Password Management. For real, get secure password management it can really help to improve your digital life experience and this tool called BitWarden is free](https://vault.bitwarden.com/#/register?layout=default).
3. Read up on the importance of [2-factor authentication](https://duo.com/product/multi-factor-authentication-mfa/two-factor-authentication-2fa) (I recommend 2fa for *all* your online accounts).
4. [The value of good project management](https://medium.com/@hasniarif/how-to-use-gitlab-as-a-project-management-tool-1496e22bb033).
5. [Beacon Food Forest Group/Project Organizational Chart (GitLab)](./bff_org_chart.pdf)
6. [Member joining BFF GitLab flow chart](./new_user_flow.pdf)
7. Ask questions (my email is in the thread where this proposal was shared)

#### Organizationally, what will it look like?

People will join through a lens of the Beacon Food Forest [Commitment to Anti-Racism](https://beaconfoodforest.org/peoplecare/commitmenttoantiracism) and [Code of Conduct](https://beaconfoodforest.org/peoplecare/codeofconduct). With this in mind, we are helping to develop trackable history of new membership that aren't one-off email requests. With GitLab, [Service Desk feature](https://docs.gitlab.com/ee/user/project/service_desk.html) can be used to convert would-be members' emails they send to a specific email address forwarder and voila! A trackable [Issue](https://docs.gitlab.com/ee/user/project/issues/) has been created in your `New Member Join` project created for onboarding new users.

Administrators (or, "arbiters" as I have been calling them) follow a naming convention such as `arbiter-stw` for Arbiter of Steward project, or `arbiter-polln8` for Pollinator subgroup, and so on. The main owner so far is an owner account called `beaconfoodarbiter` and can transfer ownership to the corresponding/to-be-created arbiter accounts.

[Arbiter](https://www.merriam-webster.com/dictionary/arbitrator) - `community-owned account made for authentication/merging changes pertaining to community-derived/owned material. A collective and/or individual`

For each arbiter account, it will be saved in an owner's vault within BitWarden held by the main owner account, `beaconfoodarbiter`. Co-owner to all assets will be Cherry and whomever maintains in her position as community harbinger/leader. This is to maintain transparency in arbiter functions to the dedicated person of focus on Beacon Food Forest. 

Per subgroup and/or project within the greater Beacon Food Forest group there can be adjustable memberships. I.e. [invite a user as a guest, maintainer, reporter, etc.](https://docs.gitlab.com/ee/user/permissions.html) and agreed-upon community guidelines per team/project can be defined [through community members suggesting changes to files by making merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) and come to a consensus upon [discussing said proposed merge requests within a comment thread](https://docs.gitlab.com/ee/user/discussions/).

[For an organizational-outlook on the group/subgroup/project breakdown go here.](./bff_org_chart.pdf)


#### Financial Information

For most uses, GitLab is completely free. For more advanced use cases (typically DevOps & software development) there is [a pricing tier](https://about.gitlab.com/pricing/). For a more comprehensive list of features of GitLab & what price tiers include what features, please go [here](https://about.gitlab.com/features/)

![pricing](./image.png)

For Enterprise features, I recommend we contact GitLab sales about NGO pricing/giveaways. If we find that we need Enterprise versions, I am happy to discuss me donating funds in order to make that happen and what the scope of that would be.


#### Conclusion

GitLab is a powerful project & group management tool which is used by developers worldwide. It has much visibility in its workflows which will provide an active transparency over projects and all work that goes into them. It is asynchronous, in that should you wish to be updated on what work teammates have done, you have but to check that team's repository, rather than dig and search through a haystack of emails. That, and it has the capability of connecting to Discord, to allow for even more direct transparency of updates to work and all documents thereof.



